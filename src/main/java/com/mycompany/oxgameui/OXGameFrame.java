/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxgameui;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class OXGameFrame extends javax.swing.JFrame {

    /**
     * Creates new form OXGameFrame
     */
    public OXGameFrame() {
        x = new Player('X');
        o = new Player('O');
        load();
        table = new Table(x,o);
        initComponents();
        hideNewGameButton();
        showTable();
        showStat();
        showTurn();
    }

    public void hideNewGameButton() {
        btnNewGame.setVisible(false);
    }
       public void showNewGameButton() {
        btnNewGame.setVisible(true);
    }
       public void showStat() {
        txtXWin.setText(""+x.getWin());
        txtXLose.setText(""+x.getLose());
        txtXDraw.setText(""+x.getDraw());
        txtOWin.setText(""+o.getWin());
        txtOLose.setText(""+o.getLose());
        txtODraw.setText(""+o.getDraw());
    }
       public void showTurn(){
           if(table.getCurrentPlayer().getName() == 'X'){
               txtPlayerX.setBackground(Color.green);
               txtPlayerO.setBackground(Color.white);
           }
           else{
               txtPlayerO.setBackground(Color.green);
               txtPlayerX.setBackground(Color.white);
               
           }
       }
       
       public void resetTurn(){
           txtPlayerX.setBackground(Color.white);
           txtPlayerO.setBackground(Color.white);
       }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlTable = new javax.swing.JPanel();
        btnTable1 = new javax.swing.JButton();
        btnTable2 = new javax.swing.JButton();
        btnTable3 = new javax.swing.JButton();
        btnTable4 = new javax.swing.JButton();
        btnTable5 = new javax.swing.JButton();
        btnTable6 = new javax.swing.JButton();
        btnTable7 = new javax.swing.JButton();
        btnTable8 = new javax.swing.JButton();
        btnTable9 = new javax.swing.JButton();
        txtMessage = new javax.swing.JLabel();
        btnNewGame = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        txtPlayerX = new javax.swing.JLabel();
        jlb2 = new javax.swing.JLabel();
        jlb3 = new javax.swing.JLabel();
        jlb4 = new javax.swing.JLabel();
        txtXWin = new javax.swing.JLabel();
        txtXLose = new javax.swing.JLabel();
        txtXDraw = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jlb9 = new javax.swing.JLabel();
        jlb10 = new javax.swing.JLabel();
        jlb11 = new javax.swing.JLabel();
        txtPlayerO = new javax.swing.JLabel();
        txtOWin = new javax.swing.JLabel();
        txtOLose = new javax.swing.JLabel();
        txtODraw = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnTable1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable1.setText("-");
        btnTable1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable1ActionPerformed(evt);
            }
        });

        btnTable2.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable2.setText("-");
        btnTable2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable2ActionPerformed(evt);
            }
        });

        btnTable3.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable3.setText("-");
        btnTable3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable3ActionPerformed(evt);
            }
        });

        btnTable4.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable4.setText("-");
        btnTable4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable4ActionPerformed(evt);
            }
        });

        btnTable5.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable5.setText("-");
        btnTable5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable5ActionPerformed(evt);
            }
        });

        btnTable6.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable6.setText("-");
        btnTable6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable6ActionPerformed(evt);
            }
        });

        btnTable7.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable7.setText("-");
        btnTable7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable7ActionPerformed(evt);
            }
        });

        btnTable8.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable8.setText("-");
        btnTable8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable8ActionPerformed(evt);
            }
        });

        btnTable9.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        btnTable9.setText("-");
        btnTable9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTable9ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlTableLayout = new javax.swing.GroupLayout(pnlTable);
        pnlTable.setLayout(pnlTableLayout);
        pnlTableLayout.setHorizontalGroup(
            pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlTableLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlTableLayout.createSequentialGroup()
                        .addComponent(btnTable1)
                        .addGap(14, 14, 14)
                        .addComponent(btnTable2)
                        .addGap(18, 18, 18)
                        .addComponent(btnTable3))
                    .addGroup(pnlTableLayout.createSequentialGroup()
                        .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlTableLayout.createSequentialGroup()
                                .addComponent(btnTable7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnTable8))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlTableLayout.createSequentialGroup()
                                .addComponent(btnTable6)
                                .addGap(14, 14, 14)
                                .addComponent(btnTable4)))
                        .addGap(18, 18, 18)
                        .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnTable5)
                            .addComponent(btnTable9))))
                .addGap(38, 38, 38))
        );
        pnlTableLayout.setVerticalGroup(
            pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTableLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTable1)
                    .addComponent(btnTable2)
                    .addComponent(btnTable3))
                .addGap(18, 18, 18)
                .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTable4)
                    .addComponent(btnTable5)
                    .addComponent(btnTable6))
                .addGap(18, 18, 18)
                .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTable7)
                    .addComponent(btnTable8)
                    .addComponent(btnTable9))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        txtMessage.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtMessage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtMessage.setText(" Welcome to OX Game");

        btnNewGame.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        btnNewGame.setText("New Game");
        btnNewGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewGameActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        txtPlayerX.setBackground(new java.awt.Color(255, 255, 255));
        txtPlayerX.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtPlayerX.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtPlayerX.setText("X");
        txtPlayerX.setOpaque(true);

        jlb2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlb2.setText("Win : ");

        jlb3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlb3.setText("Lose : ");

        jlb4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlb4.setText("Draw : ");

        txtXWin.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtXWin.setText("0");

        txtXLose.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtXLose.setText(" 0");

        txtXDraw.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtXDraw.setText("0");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPlayerX, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jlb4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtXDraw))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jlb3)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(txtXLose, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jlb2)
                                    .addGap(18, 18, 18)
                                    .addComponent(txtXWin, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 151, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtPlayerX, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlb2)
                    .addComponent(txtXWin))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlb3)
                    .addComponent(txtXLose))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlb4, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtXDraw))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jlb9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlb9.setText("Win : ");

        jlb10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlb10.setText("Lose : ");

        jlb11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlb11.setText("Draw : ");

        txtPlayerO.setBackground(new java.awt.Color(255, 255, 255));
        txtPlayerO.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtPlayerO.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtPlayerO.setText("O");
        txtPlayerO.setOpaque(true);

        txtOWin.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtOWin.setText(" 0");

        txtOLose.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtOLose.setText("0");

        txtODraw.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtODraw.setText("0");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPlayerO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlb10)
                                    .addComponent(jlb11))
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addComponent(txtODraw))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtOLose))))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jlb9)
                                .addGap(18, 18, 18)
                                .addComponent(txtOWin, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtPlayerO, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jlb9)
                            .addComponent(txtOWin))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jlb10)
                            .addComponent(txtOLose))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jlb11, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(65, 65, 65)
                        .addComponent(txtODraw)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtMessage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(pnlTable, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnNewGame, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(pnlTable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNewGame, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addComponent(txtMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnTable1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable1ActionPerformed
        row = 0;
        col = 0;
        process();
    }//GEN-LAST:event_btnTable1ActionPerformed

    private void btnTable2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable2ActionPerformed
        row = 0;
        col = 1;
        process();
    }//GEN-LAST:event_btnTable2ActionPerformed

    private void btnTable3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable3ActionPerformed
        row = 0;
        col = 2;
        process();
    }//GEN-LAST:event_btnTable3ActionPerformed

    private void btnTable4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable4ActionPerformed
        row = 1;
        col = 1;
        process();
    }//GEN-LAST:event_btnTable4ActionPerformed

    private void btnTable5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable5ActionPerformed
        row = 1;
        col = 2;
        process();
    }//GEN-LAST:event_btnTable5ActionPerformed

    private void btnTable6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable6ActionPerformed
        row = 1;
        col = 0;
        process();
    }//GEN-LAST:event_btnTable6ActionPerformed

    private void btnTable7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable7ActionPerformed
        row = 2;
        col = 0;
        process();
    }//GEN-LAST:event_btnTable7ActionPerformed

    private void btnTable8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable8ActionPerformed
        row = 2;
        col = 1;
        process();
    }//GEN-LAST:event_btnTable8ActionPerformed

    private void btnTable9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTable9ActionPerformed
        row = 2;
        col = 2;
        process();
    }//GEN-LAST:event_btnTable9ActionPerformed

    private void btnNewGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewGameActionPerformed
        newTable();
        showTable();
        showTurn();
        showWlecomeMessage();
    }//GEN-LAST:event_btnNewGameActionPerformed

    public void newTable() {
        int randInt = 0 + (int)(Math.random() * ((100-0)+1));
        if(randInt%2==0){
            table = new Table(x,o);
        }
        else{
            table = new Table(o,x);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(OXGameFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(OXGameFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(OXGameFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(OXGameFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new OXGameFrame().setVisible(true);
            }
        });
    }
    private void process(){
        if(table.setRowCol(row,col)){
            if(table.isFinish()){
                showResult();
                showStat();
                resetTurn();
                showNewGameButton();
                save();
            }else{
             table.switchPlayer();
             showTurn();
            }
            
        }
        showTable();
    }

    public void showResult() {
        if(table.getWinner() == null){
            showDraw();
        }
        else{
            showWin();
        }
    }

    public void showWin() {
        txtMessage.setText(table.getWinner().getName() + " WIN!!!!");
    }

    public void showDraw() {
        txtMessage.setText("DRAW !!!!");
    }
    
    private void showTable(){
        btnTable1.setText(""+table.getRowCol(0,0));
        btnTable2.setText(""+table.getRowCol(0,1));
        btnTable3.setText(""+table.getRowCol(0,2));
        btnTable4.setText(""+table.getRowCol(1,1));
        btnTable5.setText(""+table.getRowCol(1,2));
        btnTable6.setText(""+table.getRowCol(1,0));
        btnTable7.setText(""+table.getRowCol(2,0));
        btnTable8.setText(""+table.getRowCol(2,1));
        btnTable9.setText(""+table.getRowCol(2,2));
    }
    public void save(){
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream  oos = null;
        try {
            file = new File("ox.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(x);
            oos.writeObject(o);
            oos.close();
            fos.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestWriteObject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestWriteObject.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                   oos.close();
                   fos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestWriteObject.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public void load(){
           File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        Player x = null;
        Player o = null;
        
        
        try {
            file = new File("ox.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            x = (Player)ois.readObject();
            o = (Player)ois.readObject();
            System.out.println(x);
            System.out.println(o);
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestReadObject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestReadObject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestReadObject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private int row, col;
    private Table table;
    private Player x;
    private Player o;
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNewGame;
    private javax.swing.JButton btnTable1;
    private javax.swing.JButton btnTable2;
    private javax.swing.JButton btnTable3;
    private javax.swing.JButton btnTable4;
    private javax.swing.JButton btnTable5;
    private javax.swing.JButton btnTable6;
    private javax.swing.JButton btnTable7;
    private javax.swing.JButton btnTable8;
    private javax.swing.JButton btnTable9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel jlb10;
    private javax.swing.JLabel jlb11;
    private javax.swing.JLabel jlb2;
    private javax.swing.JLabel jlb3;
    private javax.swing.JLabel jlb4;
    private javax.swing.JLabel jlb9;
    private javax.swing.JPanel pnlTable;
    private javax.swing.JLabel txtMessage;
    private javax.swing.JLabel txtODraw;
    private javax.swing.JLabel txtOLose;
    private javax.swing.JLabel txtOWin;
    private javax.swing.JLabel txtPlayerO;
    private javax.swing.JLabel txtPlayerX;
    private javax.swing.JLabel txtXDraw;
    private javax.swing.JLabel txtXLose;
    private javax.swing.JLabel txtXWin;
    // End of variables declaration//GEN-END:variables

    private void showWlecomeMessage() {
        txtMessage.setText(" Welcome to OX Game");
    }
}
