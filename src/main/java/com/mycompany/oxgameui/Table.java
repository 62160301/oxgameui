/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxgameui;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class Table implements Serializable {

    private char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private int lastCol, lastRow;
    private Player winner;
    private boolean finish = false;
    int checkTurn = 0;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }

    public void showTable() {
        System.out.println(" 123");
        for (int i = 0; i < table.length; i++) {
            System.out.print((i + 1));
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println("");
        }
    }
    
    public char getRowCol(int row,int col){
        return table[row][col];
    }
    

    public boolean setRowCol(int row, int col) {
        if(isFinish()) return false;
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastRow = row;
            this.lastCol = col;
            checkTurn++;
            checkWin();
            return true;
        }
        return false;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }

    }

    public void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastCol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerX.win();
            playerO.lose();
        }
    }

    public void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastRow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    public void checkX() {
        if (table[0][0] == currentPlayer.getName() && table[1][1] == currentPlayer.getName() && table[2][2] == currentPlayer.getName()) {
            finish = true;
            winner = currentPlayer;
            setStatWinLose();
        } else if (table[0][2] == currentPlayer.getName() && table[1][1] == currentPlayer.getName() && table[2][0] == currentPlayer.getName()) {
            finish = true;
            winner = currentPlayer;
            setStatWinLose();
        }

    }

    public void checkDraw() {
        if (checkTurn == 9) {
            playerX.draw();
            playerO.draw();
            finish = true;
            winner = null;
        }

    }

    public void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkDraw();
    }

    public boolean isFinish() {
        return finish;
    }

    public Player getWinner() {
        return winner;
    }

    public void setTurn() {
        checkTurn = 0;
    }

}
